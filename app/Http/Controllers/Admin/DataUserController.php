<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\AddressUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\AddUserRequest;

class DataUserController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'admin']);
    }
    
    public function index()
    {
        $users = User::get();
        $titlePage = "Data User";
        $breadcrumbs = array(
            (object) [
                'title' => 'Data User',
                'link' => '',
            ],
        );
        return view('admin.data-user.index', compact('users', 'titlePage', 'breadcrumbs'));
    }

    public function showAddUserView()
    {
        
        $titlePage = "Add Data User";
        $breadcrumbs = array(
            (object) [
                'title' => 'Data User',
                'link' => 'admin.data-user',
            ], (object)
            [
                'title' => 'Add Data User',
                'link' => '',
            ],
        );
        $user = new User();
        $address = new AddressUser();
        return view('admin.data-user.add-user', compact('user', 'address', 'titlePage', 'breadcrumbs'));
    }

    public function store(AddUserRequest $request)
    {
        $slug = Str::slug($request->name);

        if(request()->file('photo')) {
            $photo = request()->file('photo');
            $photoName = "{$slug}.{$photo->extension()}";
            $photoUrl = $photo->storeAs("images/user-photos", $photoName);
        } else {
            $photoName = null;
        }

        $user = User::create([
            'email' => $request->email,
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'photo' => $photoName,
            'role' => $request->role,
        ]);

        $address = $user->addressUsers()->create([
            'title' => $request->title,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'address' => $request->address,
            'postal_code' => $request->postalCode,
        ]);

        request()->session()->flash('success', 'The user has been successfully created');

        return redirect(route('admin.data-user'));
    }

    public function destroy(User $user)
    {
        Storage::delete($user->photo);
        $user->delete();
        request()->session()->flash('success', 'The user was deleted');
        return redirect(route('admin.data-user'));
    }
}
