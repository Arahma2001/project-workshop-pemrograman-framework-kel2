<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeControllerUnauthenticated extends Controller
{
    public function index()
    {
        return view('user.home');
    }
}
