<?php

namespace App\Http\Controllers\User;

use App\Models\Chocolate;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{

    public function index()
    {
        dd(now()->format('d-m-Y'));
        return view('user.shop', [
            'chocolates' => Chocolate::get(),
        ]);
    }

}
