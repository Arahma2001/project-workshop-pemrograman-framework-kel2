<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users,email',
            'name' => 'required|max:255',
            'password' => 'required|min:8',
            'confirmPassword' => 'required|same:password|min:8',
            'photo' => 'required|image|max:2048|mimes:png,jpg,jpeg',
            'role' => 'required',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'postalCode' => 'required|numeric',
        ];
    }
}
