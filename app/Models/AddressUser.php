<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressUser extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'address', 'province', 'city', 'district', 'postal_code'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
