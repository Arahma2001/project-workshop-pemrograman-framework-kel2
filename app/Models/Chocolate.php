<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chocolate extends Model
{
    use HasFactory;

    public function photos()
    {
        return $this->hasMany(ChocolatePhoto::class);
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class);
    }
}
