<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChocolatePhoto extends Model
{
    use HasFactory;

    public $timestamps = false;

    
    public function chocolate()
    {
        return $this->belongsTo(Chocolate::class);
    }
}
