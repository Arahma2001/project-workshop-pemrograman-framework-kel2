<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function isAdmin() {
        return $this->role === 'admin';
    }
 
    public function isUser() {
        return $this->role === 'user';
    }

    public function cart()
    {
        return $this->hasOne(User::class);
    }

    public function addressUsers()
    {
        return $this->hasMany(AddressUser::class);
    }

    public function getTakePhotoAttribute()
    {
        return $this->photo == null ? '/storage/images/user-photos/default_user.jpg' : '/storage/images/user-photos/' . $this->photo;
    }

    // public function transactions()
    // {
    //     return $this->belongsToMany(Transaction::class);
    // }
}
