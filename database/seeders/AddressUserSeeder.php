<?php

namespace Database\Seeders;

use App\Models\AddressUser;
use Illuminate\Database\Seeder;

class AddressUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addresses = [
            [
                'user_id' => 1,
                'title' => 'Rumah Admin',
                'province' => 'Jawa Timur',
                'city' => 'Lamongan',
                'district' => 'Lamongan',
                'address' => 'Sidomulyo Utara Gg. Himalaya',
                'postal_code' => '62217'
            ], [
                'user_id' => 2,
                'title' => 'Rumah Bima',
                'province' => 'Jawa Timur',
                'city' => 'Sidoarjo',
                'district' => 'Waru',
                'address' => 'Jl. Garuda No. 13',
                'postal_code' => '201312312'
            ], [
                'user_id' => 2,
                'title' => 'Rumah Farrel',
                'province' => 'Jawa Timur',
                'city' => 'Sidoarjo',
                'district' => 'Pepe Legi',
                'address' => 'Jl. Elang No. 99',
                'postal_code' => '82132141'
            ]
        ];

        foreach($addresses as $address) {
            AddressUser::create($address);
        }
    }
}
