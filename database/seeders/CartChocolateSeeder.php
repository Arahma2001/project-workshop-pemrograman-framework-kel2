<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartChocolateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cartChocolates = [
            [
                'cart_id' => 1,
                'chocolate_id' => 2,
                'quantity' => 1,
            ], [
                'cart_id' => 1,
                'chocolate_id' => 12,
                'quantity' => 1,
            ], [
                'cart_id' => 2,
                'chocolate_id' => 1,
                'quantity' => 2,
            ],
        ];
        foreach($cartChocolates as $cartChocolate) {
            DB::table('cart_chocolate')->insert($cartChocolate);
        }
    }
}
