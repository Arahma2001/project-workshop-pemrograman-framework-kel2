<?php

namespace Database\Seeders;

use App\Models\Cart;
use Illuminate\Database\Seeder;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carts = [
            [
                'user_id' => 1,
                'status' => 'uncheckout',
            ], [
                'user_id' => 2,
                'status' => 'checkouted',
            ],
        ];
        foreach($carts as $cart) {
            Cart::create($cart);
        }
    }
}
