<?php

namespace Database\Seeders;

use App\Models\ChocolatePhoto;
use Illuminate\Database\Seeder;

class ChocolatePhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chocolatePhotos = [
            [
                'chocolate_id' => 1,
                'photo' => 'img/product/chocobox1.jpg',
            ], [
                'chocolate_id' => 1,
                'photo' => 'img/product/choco1.jpg',
            ], [
                'chocolate_id' => 2,
                'photo' => 'img/product/chocobox2.jpg',
            ], [
                'chocolate_id' => 2,
                'photo' => 'img/product/choco2.jpg',
            ], [
                'chocolate_id' => 3,
                'photo' => 'img/product/chocobox3.jpg',
            ], [
                'chocolate_id' => 3,
                'photo' => 'img/product/choco3.jpg',
            ], [
                'chocolate_id' => 4,
                'photo' => 'img/product/chocobox4.jpg',
            ], [
                'chocolate_id' => 4,
                'photo' => 'img/product/choco4.jpg',
            ], [
                'chocolate_id' => 5,
                'photo' => 'img/product/chocobox5.jpg',
            ], [
                'chocolate_id' => 5,
                'photo' => 'img/product/choco5.jpg',
            ], [
                'chocolate_id' => 6,
                'photo' => 'img/product/chocobox6.jpg',
            ], [
                'chocolate_id' => 6,
                'photo' => 'img/product/choco6.jpg',
            ], [
                'chocolate_id' => 7,
                'photo' => 'img/product/chocobox7.jpg',
            ], [
                'chocolate_id' => 7,
                'photo' => 'img/product/choco7.jpg',
            ], [
                'chocolate_id' => 8,
                'photo' => 'img/product/chocobox8.jpg',
            ], [
                'chocolate_id' => 8,
                'photo' => 'img/product/choco8.jpg',
            ], [
                'chocolate_id' => 9,
                'photo' => 'img/product/chocobox9.jpg',
            ], [
                'chocolate_id' => 9,
                'photo' => 'img/product/choco9.jpg',
            ], [
                'chocolate_id' => 10,
                'photo' => 'img/product/chocobox10.jpg',
            ], [
                'chocolate_id' => 10,
                'photo' => 'img/product/choco10.jpg',
            ], [
                'chocolate_id' => 11,
                'photo' => 'img/product/chocobox11.jpg',
            ], [
                'chocolate_id' => 11,
                'photo' => 'img/product/choco11.jpg',
            ], [
                'chocolate_id' => 12,
                'photo' => 'img/product/chocobox12.jpg',
            ], [
                'chocolate_id' => 12,
                'photo' => 'img/product/choco12.jpg',
            ]
        ];
        foreach($chocolatePhotos as $chocolatePhoto) {
            ChocolatePhoto::create($chocolatePhoto);
        }
    }
}
