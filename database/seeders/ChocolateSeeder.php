<?php

namespace Database\Seeders;

use App\Models\Chocolate;
use Illuminate\Database\Seeder;

class ChocolateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chocolates = [
            [
                'name' => 'Gift Box of 12 Truffles',
                'slug' => 'gift-box-of-12-truffles',
                'weight' => 50,
                'price' => 571000,
                'discount' => 0.85,
            ], [
                'name' => 'White Chocolate Truffles',
                'slug' => 'white-chocolate-truffles',
                'weight' => 50,
                'price' => 499000,
            ], [
                'name' => 'White Chocolate & Nuts Truffles',
                'slug' => 'white-chocolate-and-nuts-truffles',
                'weight' => 50,
                'price' => 528000,
            ], [
                'name' => 'Milk Chocolate Truffles',
                'slug' => 'milk-chocolate-truffles',
                'weight' => 50,
                'price' => 571000,
            ], [
                'name' => 'Milk Chocolate & Coconut Truffles',
                'slug' => 'milk-chocolate-and-coconut-truffles',
                'weight' => 50,
                'price' => 528000,
            ], [
                'name' => 'Raspberry Truffles',
                'slug' => 'raspberry-truffles',
                'weight' => 50,
                'price' => 528000,
                'discount' => 0.9,
            ], [
                'name' => 'Lavender Chocolate Truffles',
                'slug' => 'lavender-chocolate-truffles',
                'weight' => 50,
                'price' => 499000,
            ], [
                'name' => 'Dark Chocolate Truffles',
                'slug' => 'dark-chocolate-truffles',
                'weight' => 50,
                'price' => 571000,
            ], [
                'name' => 'Peanut Butter Truffles',
                'slug' => 'peanut-butter-truffles',
                'weight' => 50,
                'price' => 542000,
            ], [
                'name' => 'Pecan Pie Truffles',
                'slug' => 'pecan-pie-truffles',
                'weight' => 50,
                'price' => 642000,
            ], [
                'name' => 'Apple Sauce Truffles',
                'slug' => 'apple-sauce-truffles',
                'weight' => 50,
                'price' => 614000,
            ], [
                'name' => 'Mocha Brownie Truffles',
                'slug' => 'mocha-brownie-truffles',
                'weight' => 50,
                'price' => 457000,
            ]
        ];
        foreach($chocolates as $chocolate) {
            Chocolate::create($chocolate);
        }
    }
}
