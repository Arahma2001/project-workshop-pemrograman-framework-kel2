<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(AddressUserSeeder::class);
        $this->call(ChocolateSeeder::class);
        $this->call(ChocolatePhotoSeeder::class);
        $this->call(CartSeeder::class);
        $this->call(CartChocolateSeeder::class);
        $this->call(TransactionSeeder::class);
    }
}
