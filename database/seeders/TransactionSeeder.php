<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactions = [
            [
                'user_id' => 1,
                'cart_id' => 1,
                'code' => Str::random(15),
                'status' => 'unpaid',
                'total_price' => 956000,
                'shipping' => 15000,
                'destination' => 'Jl. Rajawali Indah No.3, Waru, Sidoarjo',
                'created_at' => now()->format('Y-m-d'),
            ], [
                'user_id' => 2,
                'cart_id' => 2,
                'code' => Str::random(15),
                'status' => 'processed',
                'total_price' => 1142000,
                'shipping' => 15000,
                'destination' => 'Jl. Garuda Timur No.14, Waru, Sidoarjo',
                'created_at' => now()->format('Y-m-d'),
            ],
        ];
        foreach($transactions as $transaction) {
            Transaction::create($transaction);
        }
    }
}
