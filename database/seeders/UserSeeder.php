<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('admin'),
                'role' => 'admin',
            ], [
                'name' => 'bima',
                'email' => 'bima@gmail.com',
                'password' => Hash::make('bima2001'),
                'role' => 'user',
            ]
        ];
        foreach($users as $user) {
            User::create($user);
        }
    }
}
