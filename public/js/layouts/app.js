const hamburgerButtonNavbar = document.querySelector('.hamburger-navbar');
const closeButtonNavbar = document.querySelector('.close-navbar-mobile');
const navbarMobile = document.querySelector('.navbar-mobile');
const navbarOverlay = document.querySelector('.overlay-navbar-mobile');

function openNavbarMobile() {
    navbarMobile.classList.add('show');
    navbarOverlay.classList.add('show');
    document.querySelector('body').classList.add('show');
}

function closeNavbarMobile() {
    navbarMobile.classList.remove('show');
    navbarOverlay.classList.remove('show');
    document.querySelector('body').classList.remove('show');
}

hamburgerButtonNavbar.addEventListener('click', openNavbarMobile);
closeButtonNavbar.addEventListener('click', closeNavbarMobile);