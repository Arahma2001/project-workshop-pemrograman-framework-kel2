new ActiveMenuLink(".navbar",{

    // selector of menu item
    itemTag: ".scrollspy",

    // active class
    activeClass: "active",

    // in pixels
    scrollOffset: -100,

    // duration in ms
    scrollDuration: 500,

    // easing function
    ease: "out-circ",

    // specifies the header height
    headerHeight: 72,

    // string
    default: "#home",

    // shows hash tag
    showHash: false

});
