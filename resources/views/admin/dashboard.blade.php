@extends('admin.layouts.app')

@section('card-stats')
    @include('admin.layouts.card-stats')
@endsection

@section('content')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card bg-default">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-light text-uppercase ls-1 mb-1">Overview</h6>
                                <h5 class="h3 text-white mb-0">Sales Value</h5>
                            </div>
                            <div class="col">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart"
                                        data-target="#chart-sales-dark"
                                        data-update='{"data":{"datasets":[{"data":[0, 200, 100, 300, 150, 400, 200, 600, 600]}]}}'
                                        data-prefix="Rp. " data-suffix="k">
                                        <span class="nav-link py-2 px-3 active">
                                            <span class="d-none d-md-block">Month</span>
                                            <span class="d-md-none">M</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <!-- Chart wrapper -->
                            <canvas id="chart-sales-dark" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>
                                <h5 class="h3 mb-0">Total Orders</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <canvas id="chart-bars" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Last Transaction</h3>
                            </div>
                            <div class="col text-right">
                                <a href="#!" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Total Price</th>
                                    <th scope="col">Transaction Date</th>
                                    <th scope="col">Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="w-20" scope="row">
                                        Budi Santono
                                    </th>
                                    <td class="w-20">
                                        100.000
                                    </td>
                                    <td class="w-20">
                                        24 February 2021
                                    </td>
                                    <td class="white-space-normal">
                                        Jl. Kacang Kapri Muda Kav. 13, Utan Kayu Selatan, Matraman, Jakarta Timur,
                                        Indonesia, 13120
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w-20" scope="row">
                                        Bima Aurasakti
                                    </th>
                                    <td class="w-20">
                                        1.000.000
                                    </td>
                                    <td class="w-20">
                                        26 February 2021
                                    </td>
                                    <td class="white-space-normal">
                                        Jl. Kacang Polong Kav. 14, Utan Kayu Utara, Matraman, Jakarta Timur,
                                        Indonesia, 13120
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w-20" scope="row">
                                        Yoshi Virgiawan
                                    </th>
                                    <td class="w-20">
                                        200.000
                                    </td>
                                    <td class="w-20">
                                        12 May 2021
                                    </td>
                                    <td class="white-space-normal">
                                        Jl. Kacang Garuda No. 13, Utan Kayu Timur, Matraman, Jakarta Timur,
                                        Indonesia, 13120
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w-20" scope="row">
                                        Budi Santono
                                    </th>
                                    <td class="w-20">
                                        100.000
                                    </td>
                                    <td class="w-20">
                                        24 February 2021
                                    </td>
                                    <td class="white-space-normal">
                                        Jl. Kacang Kapri Muda Kav. 13, Utan Kayu Selatan, Matraman, Jakarta Timur,
                                        Indonesia, 13120
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w-20" scope="row">
                                        Budi Santono
                                    </th>
                                    <td class="w-20">
                                        100.000
                                    </td>
                                    <td class="w-20">
                                        24 February 2021
                                    </td>
                                    <td class="white-space-normal">
                                        Jl. Kacang Kapri Muda Kav. 13, Utan Kayu Selatan, Matraman, Jakarta Timur,
                                        Indonesia, 13120
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Chocolate Sold</h3>
                            </div>
                            <div class="col text-right">
                                <a href="#!" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Amount Sold</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        White Chocolate Truffles
                                    </th>
                                    <td>
                                        5.480
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        White Chocolate & Nuts Truffles
                                    </th>
                                    <td>
                                        4.807
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Milk Chocolate Truffles
                                    </th>
                                    <td>
                                        3.678
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Milk Chocolate & Coconut Truffles
                                    </th>
                                    <td>
                                        2.645
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Raspberry Truffles
                                    </th>
                                    <td>
                                        1.480
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Lavender Chocolate Truffles
                                    </th>
                                    <td>
                                        1.080
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Dark Chocolate Truffles
                                    </th>
                                    <td>
                                        853
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Peanut Butter Truffles
                                    </th>
                                    <td>
                                        491
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        @include('admin.layouts.footer')
    </div>
@endsection