@extends('admin.layouts.app')

@section('optional-css')
    <link rel="stylesheet" href="{{ asset('css/admin/data-user/add-user.css') }}">
@endsection

@section('content')
<div class="container-fluid mt--6">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Edit profile </h3>
                </div>
                {{-- <div class="col-4 text-right">
                    <a href="#!" class="btn btn-sm btn-primary">Settings</a>
                </div> --}}
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.data-user.store') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @csrf
                @include('admin.data-user.partials.form-control', ['action' => 'add'])
            </form>
        </div>
    </div>
</div>
@endsection
