@extends('admin.layouts.app')

@section('optional-css')
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('css/admin/magnific-popup/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <link rel="stylesheet" href="{{ asset('css/admin/data-user/index.css') }}">
@endsection

@section('optional-action')
    <div class="col-lg-6 col-5 text-right">
        <a href="{{ route('admin.data-user.add') }}" class="btn btn-sm btn-neutral">Add User</a>
    </div>
@endsection

@section('content')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">Data User</h3>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="photo">Photo</th>
                                    <th scope="col" class="sort" data-sort="name">Name</th>
                                    <th scope="col" class="sort" data-sort="email">Email</th>
                                    <th scope="col" class="sort" data-sort="address">Address</th>
                                    <th scope="col" class="sort" data-sort="role">Role</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($users as $user)
                                    <tr>
                                        <th scope="row">
                                            <a href="{{ $user->takePhoto }}" class="image-link avatar rounded-circle mr-3">
                                                <img alt="Image placeholder" src="{{ $user->takePhoto }}">
                                            </a>
                                        </th>
                                        <td class="email">
                                            {{ $user->name }}
                                        </td>
                                        <td class="email">
                                            {{ $user->email }}
                                        </td>
                                        <td class="email">
                                            {{ $user->addressUsers[0]->address . ", " . $user->addressUsers[0]->district . ", " . $user->addressUsers[0]->city . ", " . $user->addressUsers[0]->province }}
                                        </td>
                                        <td class="email">
                                            {{ $user->role }}
                                        </td>
                                        <td class="text-left">
                                            <div>
                                                <button class="btn btn-sm btn-icon-only text-light mr-0" type="button"
                                                    data-toggle="modal" data-target="{{ '#modal-default' . $user->id }}">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <div class="modal fade" id="{{ 'modal-default' . $user->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                                                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                                                        <div class="modal-content">
                                                            
                                                            <div class="modal-header">
                                                                <h6 class="modal-title" id="modal-title-default">Detail Data User</h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            
                                                            <div class="modal-body">
                                                                
                                                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                                                    <div class="pl-lg-4">
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-email">Email Address</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-name">Name</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-first-name">Password</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-last-name">Confirm Password</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-photo">Photo</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-role">Role</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr class="my-4" />
                                                                    <!-- Address -->
                                                                    <h6 class="heading-small text-muted mb-4">Address information</h6>
                                                                    <div class="pl-lg-4">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-title">Title</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-address">Address</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-province">Province</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-city">City</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-district">District</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="form-group">
                                                                                    <label class="form-control-label" for="input-postalCode">Postal code</label>
                                                                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-sm btn-icon-only text-light mr-0" href="#"
                                                    data-toggle="modal" data-target="{{ '#modal-alert-delete' . $user->id }}">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                <div class="modal fade" id="{{ 'modal-alert-delete' . $user->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ 'modal-alert-delete' . $user->id }}" aria-hidden="true">
                                                    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
                                                        <div class="modal-content bg-gradient-danger">
                                                            
                                                            <div class="modal-header">
                                                                <h6 class="modal-title" id="modal-title-notification">Your attention is required</h6>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            
                                                            <div class="modal-body">
                                                                
                                                                <div class="py-3 text-center">
                                                                    <i class="far fa-times-circle" style="font-size: 100px; margin-bottom: 40px;"></i>
                                                                    <h4 class="heading mt-4">Are you sure you want to delete this user?</h4>
                                                                    <p>Once you delete this user, this user cannot be returned!</p>
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-link text-white" data-dismiss="modal">Cancel</button>
                                                                <form action="{{ route('admin.data-user.delete', $user->id) }}" method="post">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="submit" class="btn btn-white">Delete</button>
                                                                </form>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="#">Action</a>
                                                    <a class="dropdown-item" href="#">Another action</a>
                                                    <a class="dropdown-item" href="#">Something else here</a>
                                                </div> --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- Card footer -->
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        @include('admin.layouts.footer')
    </div>
@endsection

@section('optional-js')
    <!-- Magnific Popup core JS file -->
    <script src="{{ asset('js/admin/magnific-popup/jquery.magnific-popup.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script>
        // Initialize popup as usual
        $('.image-link').magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below

            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',

                opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    </script>
@endsection