<h6 class="heading-small text-muted mb-4">User information</h6>
<div class="pl-lg-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-email">Email Address</label>
                <input type="email" id="input-email" name="email" class="form-control" placeholder="jessy@example.com" value="{{ old('email') ?? $user->email }}">
                @error('email')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-name">Name</label>
                <input type="text" id="input-name" name="name" class="form-control" placeholder="jessy" value="{{ old('name') ?? $user->name }}">
                @error('name')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-first-name">Password</label>
                <input type="password" id="input-password" name="password" class="form-control" placeholder="Password">
                @error('password')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-last-name">Confirm Password</label>
                <input type="password" id="input-confirm-password" name="confirmPassword" class="form-control" placeholder="Confirm Password">
                @error('confirmPassword')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-photo">Photo</label>
                <input type="file" id="input-photo" name="photo" class="form-control">
                @error('photo')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-role">Role</label>
                <select class="form-control" id="input-role" name="role">
                    <option {{ old('role') == 'admin' ? 'selected' : '' }} value="admin">Admin</option>
                    <option {{ old('role') == 'user' ? 'selected' : '' }} value="user">User</option>
                </select>
                @error('role')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
</div>
<hr class="my-4" />
<!-- Address -->
<h6 class="heading-small text-muted mb-4">Address information</h6>
<div class="pl-lg-4">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label" for="input-title">Title</label>
                <input type="text" id="input-title" class="form-control" placeholder="Home Title" name="title" value="{{ old('title') ?? $address->title }}">
                @error('title')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="form-control-label" for="input-address">Address</label>
                <textarea id="input-address" class="form-control" placeholder="Home Address" name="address" rows="5">{{ old('address') ?? $address->address }}</textarea>
                @error('address')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-province">Province</label>
                <input type="text" id="input-province" class="form-control" name="province" placeholder="Province" value="{{ old('province') ?? $address->province }}">
                @error('province')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-city">City</label>
                <input type="text" id="input-city" class="form-control" placeholder="City" name="city" value="{{ old('city') ?? $address->city }}">
                @error('city')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-district">District</label>
                <input type="text" id="input-district" class="form-control" name="district" placeholder="District" value="{{ old('district') ?? $address->district }}">
                @error('district')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label" for="input-postalCode">Postal code</label>
                <input type="text" id="input-postal-code" class="form-control" name="postalCode" placeholder="Postal code" value="{{ old('postalCode') ?? $address->postalCode }}">
                @error('postalCode')
                    <div class="alert-input text-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>

    <div class="mt-4 text-right">
        <button type="submit" class="btn btn-primary">{{ $action == 'add' ? 'Add User' : 'Edit User' }}</button>
    </div>
</div>