<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <title>Admin - TrulyChoco</title>
    <!-- Favicon -->
    <link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('css/admin/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/admin/font-awesome-free/css/all.min.css') }}" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{ asset('css/admin/argon.css?v=1.2.0') }}" type="text/css">

    <!-- Optional CSS -->
    @yield('optional-css')

</head>
<body>

    @include('admin.layouts.sidebar')
    
    <!-- Main content -->
    <div class="main-content" id="panel">

        @include('admin.layouts.navbar')

        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 text-white d-inline-block mb-0">{{ $titlePage }}</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="{{ route('user.home') }}"><i class="fas fa-home"></i></a></li>
                                    @foreach ($breadcrumbs as $breadcrumb)
                                        @if ($breadcrumb != end($breadcrumbs))
                                            <li class="breadcrumb-item"><a href="{{ route($breadcrumb->link) }}">{{ $breadcrumb->title }}</a></li>
                                        @else
                                            <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb->title }}</li>
                                        @endif
                                    @endforeach
                                </ol>
                            </nav>
                        </div>

                        @yield('optional-action')
                    </div>

                    <!-- Card stats -->
                    @yield('card-stats')
                </div>
            </div>
        </div>

        @yield('content')

    </div>

    <!-- Scripts -->
    <!-- Core -->
    <script src="{{ asset('js/admin/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/admin/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/admin/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('js/admin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/admin/jquery-scroll-lock/jquery-scrollLock.min.js') }}"></script>
    {{-- <!-- Optional JS -->
    <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script> --}}
    <!-- Argon JS -->
    <script src="{{ asset('js/admin/argon.js') }}"></script>

    <!-- Optional JS -->
    @yield('optional-js')

</body>
</html>