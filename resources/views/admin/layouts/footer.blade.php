<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col">
            <div class="copyright text-center  text-lg-left  text-muted">
                &copy; 2021 <span class="font-weight-bold ml-1" target="_blank">Kelompok 2</span>
            </div>
        </div>
    </div>
</footer>