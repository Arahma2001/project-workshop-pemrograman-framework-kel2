<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="{{ asset('img/brand/logo_trulychoco.png') }}" class="navbar-brand-img" alt="...">
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('admin.dashboard') }}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.data-user') }}">
                            <i class="ni ni-single-02 text-yellow"></i>
                            <span class="nav-link-text">User</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="examples/tables.html">
                            <i class="fas fa-cookie text-primary"></i>
                            <span class="nav-link-text">Chocolate</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="examples/tables.html">
                            <i class="fas fa-shopping-basket text-info"></i>
                            <span class="nav-link-text">Transaction</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>