@extends('user.layouts.app')


@section('title')
HOME
@endsection


@section('optional-css')
<link rel="stylesheet" href="{{ asset('css/user/home.css') }}">
@endsection


@section('content')
    <!-- Home -->
<section class="scrollspy" id="home">
    <div class="w-100">
        <div class="home-background-paralax d-flex justify-content-center align-items-center flex-column">
            <div class="px-5 d-flex text-center">
                <span class="text-white me-2">-</span>
                <p class="letter-spacing-8 text-white">CHOCOLATE BOUTIQUE</p>
                <span class="text-white ms-1">-</span>
            </div>
            <h1 class="font-serif text-white display-1 mt-4">TRULYCHOCO</h1>
            <a class="home-button">
                SHOP NOW
            </a>
        </div>
    </div>
    <!-- end Home -->


    <!-- Special Offer -->
    <div class="w-100 bg-white py-5">
        <div
            data-aos="fade-up"
            data-aos-duration="1000"
            data-aos-easing="ease-in-out"
            data-aos-once="true"
            data-aos-anchor-placement="top-center"
        >
            <div class="special-offer container d-flex text-center py-5">
                <div class="special-offer-left d-flex flex-column align-items-center justify-content-center p-5">
                    <h3 class="letter-spacing-2 text-uppercase black-substitute-text-color font-serif">celebrate a special day</h3>
                    <h6 class="letter-spacing-2 word-spacing-2 text-uppercase black-substitute-text-color m-0">Limited Edition Assorted Chocolate Box</h6>
                    <div class="line-break"></div>
                    <p class="black-substitute-background-color ">
                        I'm a paragraph. Click here to add your own text and edit me. It’s easy. 
                        Just click “Edit Text” or double click me to add your own content and 
                        make changes to the font. Feel free to drag and drop me anywhere you 
                        like on your page. I’m a great place for you to tell a story and let 
                        your users know a little more about you.​
                    </p>
                    <a class="special-offer-link text-uppercase black-substitute-text-color" href="">shop all <i class="bi bi-chevron-right"></i></a>
                </div>

                <div class="special-offer-right position-relative">
                    <img src="{{ asset('img/product/chocobox1.jpg') }}" alt="">
                    <div class="outer-circle rounded-circle d-flex justify-content-center align-items-center">
                        <div class="inner-circle rounded-circle d-flex justify-content-center flex-column">
                            <p class="text-capitalize text-white m-0">Now For</p>
                            <p class="text-white m-0">$34</p>
                        </div>
                    </div>
                    <div class="text-capitalize overflow-hidden">
                        <div class="quick-view">
                            <a class="black-substitute-text-color" href="">quick View</a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Special Offer -->


    <!-- Video Parralax -->
    <div class="video-parallax-container">
        <video autoplay loop muted>
            <source src="{{ asset('video/choco.mp4') }}" type="video/mp4">
        </video>
    </div>
    <!-- end Video Parralax -->
</section>



<!-- About -->
<section class="scrollspy" id="about">
    <div class="about-container bg-white">
        <div class="container">
            <div
                data-aos="fade-up"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
                data-aos-once="true"
                data-aos-anchor-placement="top-center"
            >
                <div class="about-wrapper px-5 d-flex flex-wrap align-items-center">
                    <img src="{{ asset('img/brand/Chef.png') }}" alt="">
                    <div class="about-content text-center">
                        <h2>ABOUT</h2>
                        <h6>TRULYCHOCO</h6>
                        <div class="divider"></div>
                        <div class="about-description">
                            <p>
                                {!! nl2br("I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make changes to the font. Feel free to drag and drop me anywhere you like on your page.

                                I’m a great place for you to tell a story and let your users know a little more about you.​ This is a great space to write long text about your company and your services. You can use this space to go into a little more detail about your company. Talk about your team and what services you provide.") !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end About -->


    <!-- Quotes -->
    <div class="quotes-container">
        <div class="overlay"></div>
        <div class="container">
            <div
                data-aos="fade-up"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
                data-aos-once="true"
                data-aos-anchor-placement="top-center"
            >
                <div class="quotes-wrapper text-center">
                    <h2>“There is nothing better than a friend, unless it is a friend with chocolate.”</h2>
                    <p class="m-0">- Linda Grayson -</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end Quotes -->

    <!-- cacao Quality -->
    <div class="cacao-quality bg-white">
        <div
            data-aos="fade-up"
            data-aos-duration="1000"
            data-aos-easing="ease-in-out"
            data-aos-once="true"
            data-aos-anchor-placement="top-center"
        >
            <div class="cacao-quality-container container d-flex text-center align-items-center justify-content-between flex-wrap px-5">
                <div class="cacao-quality-left text-center">
                    <h2 class="letter-spacing-2 text-uppercase black-substitute-text-color font-serif">our cacao beans</h2>
                    <h6 class="letter-spacing-2 word-spacing-2 text-uppercase black-substitute-text-color">QUALITY CACAO FROM AROUND THE GLOBE</h6>
                    <div class="divider"></div>
                    <p class="black-substitute-background-color ">
                        I'm a paragraph. Click here to add your own text and edit me. It’s easy. 
                        Just click “Edit Text” or double click me to add your own content and make 
                        changes to the font. Feel free to drag and drop me anywhere you like on 
                        your page. I’m a great place for you to tell a story and let your users know 
                        a little more about you.​
                    </p>
                </div>

                <div class="cacao-image-container d-flex">
                    <img src="{{ asset('img/user/cacao1.jpg') }}" alt="">
                    <img src="{{ asset('img/user/cacao2.jpg') }}" class="ms-4 cacao-image-2" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- end cacao Quality -->
</section>
@endsection


@section('optional-js')
<script src="{{ asset('js/active-menu-link.js') }}"></script>
<script src="{{ asset('js/user/home.js') }}"></script>
@endsection