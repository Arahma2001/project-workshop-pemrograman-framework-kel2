<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Icon -->
        <link rel="icon" href="{{ asset('img/brand/icon_trulychoco.png') }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Bootstrap-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/layouts/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/layouts/navbar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/layouts/footer.css') }}">

        <!-- Optional CSS -->
        @yield('optional-css')

    </head>
    <body>
        <!-- Navbar -->
        @include('user.layouts.navbar')

        <!-- Content -->
        @yield('content')

        <!-- Footer -->
        @include('user.layouts.footer')
        
        <!-- Optional Javascript -->
        @yield('optional-js')

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="{{ asset('js/layouts/app.js') }}"></script>
        <script>
            AOS.init();
        </script>
    </body>
</html>
