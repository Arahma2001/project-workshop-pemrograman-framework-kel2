<div class="footer text-center bg-white">
    <div class="contact-footer" id="contact" >
        <div class="contact-footer-header">
            <h2>Contact</h2>
        </div>
        <div class="divider"></div>
        <div class="contact-footer-body">
            <p>
                <span>TEL: 0813-92923636 / INFO@TRULYCHOCO.COM</span><br>
                <span>500 TERRY FRANCOIS ST. SAN FRANCISCO, CA 94158</span><br>
                <span>OPENING HOURS 8:00 AM - 9:00 PM</span>
            </p>
        </div>
    </div>

    <div class="subscribe-footer">
        <div class="subscribe-footer-header">
            <h6>JOIN OUR MAILING LIST</h6>
        </div>
        <div class="subscribe-footer-form">
            <form action="">
                <div class="subscribe-footer-form-content d-flex justify-content-center flex-wrap">
                    <input type="email" name="email" placeholder="Enter your email address">
                    <button type="submit">Subscribe Now</button>
                </div>
            </form>
        </div>
    </div>

    <div class="creator-footer p-4">
        <p class="m-0 black-substitute-text-color">&copy;&nbsp;1985 by TrulyChoco. All rights reserved</p>
    </div>
</div>