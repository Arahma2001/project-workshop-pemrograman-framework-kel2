@extends('user.layouts.app')


@section('title')
TrulyChoco | Shop
@endsection


@section('optional-css')
<link rel="stylesheet" href="{{ asset('css/user/shop.css') }}">
@endsection


@section('content')
<div class="shop container py-5">
    <h2 class="text-uppercase black-substitute-text-color text-center">Chose Your Chocolate</h2>

    <div class="w-100 mt-5 d-flex justify-content-center flex-row flex-wrap">
        @foreach ($chocolates as $chocolate)
        <div class="shop-chocolates-container my-3">
            <div class="shop-chocolate-box">
                <div class="w-100 text-center">
                    <p class="shop-chocolate-box-title secondary-background-color py-1 px-2 text-white mb-0">Best Seller</p>
                </div>

                <div class="d-flex flex-column align-items-center position-relative">
                    <img class="shop-chocobox-photo" src="{{ asset($chocolate->photos[0]->photo) }}" alt="" width="226px">
                    <img class="shop-choco-photo" src="{{ asset($chocolate->photos[1]->photo) }}" alt="" width="226px">
                    <div class="quick-view-box overflow-hidden">
                        <div class="quick-view">
                            <a class="secondary-text-color font-size-13" href="">Quick View</a>
                        </div>
                    </div>
                    <p class="my-2 black-substitute-text-color">{{ $chocolate->name }}</p>
                    <p class="font-size-13 m-0 black-substitute-text-color">Rp.{{ number_format($chocolate->price, 0, ',', '.') }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</div>
@endsection


@section('optional-js')
<script src="{{ asset('js/user/shop.js') }}"></script>
@endsection
