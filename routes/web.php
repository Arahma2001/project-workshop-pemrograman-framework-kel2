<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\Admin\DataUserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\HomeControllerUnauthenticated;
use App\Http\Controllers\User\ShopController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [HomeControllerUnauthenticated::class, 'index'])->name('home');
Route::get('/shop', [ShopController::class, 'index'])->name('shop');

Auth::routes();

Route::group(['middleware' => ['auth', 'user']], function () {
    Route::get('/', [HomeController::class, 'index'])->name('user.home');
});

// admin protected routes
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('data-user', [DataUserController::class, 'index'])->name('admin.data-user');
    Route::get('data-user/add', [DataUserController::class, 'showAddUserView'])->name('admin.data-user.add');
    Route::post('data-user/store', [DataUserController::class, 'store'])->name('admin.data-user.store');
    Route::delete('{user:id}/delete', [DataUserController::class, 'destroy'])->name('admin.data-user.delete');
});
